﻿using System.Drawing;

namespace Asteroids_gbu
{
    interface ICollision
    {
        /// <summary>
        /// True, если прямоугольники объектов пересекаются.
        /// </summary>
        /// <param name="obj">Объект, с которым проверяется столкновение</param>
        /// <returns></returns>
        bool Collision(ICollision obj);

        /// <summary>
        /// Свойство хранит прямоугольник, по которому обсчитывается столкновение
        /// </summary>
        Rectangle Rect { get; }
    }
}
