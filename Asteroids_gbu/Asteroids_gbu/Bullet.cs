﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asteroids_gbu
{
    class Bullet : BaseObject
    {
        /// <summary>
        /// Создать игровой объект
        /// </summary>
        /// <param name="dir">Скорость и направление движения объекта</param>
        /// <param name="size">Размер игрового объекта</param>
        /// <param name="image">Картинка игрового объекта</param>
        public Bullet(Point dir, Size size, Image image) : base(dir, size, image) { InitStartPosition(); }
        /// <summary>
        /// Создать игровой объект
        /// </summary>
        /// <param name="pos">Начальная позиция объекта</param>
        /// <param name="dir">Скорость и направление движения объекта</param>
        /// <param name="size">Размер игрового объекта</param>
        public Bullet(Point pos, Point dir, Size size) : base(pos, dir, size) { }
        /// <summary>
        /// Создать игровой объект с автоматическим размером объекта по спрайту
        /// </summary>
        /// <param name="pos">Начальная позиция объекта</param>
        /// <param name="dir">Скорость и направление движения объекта</param>
        /// <param name="image">Размер картинки</param>
        public Bullet(Point pos, Point dir, Image image) : base(pos, dir, image) { }
        /// <summary>
        /// Создать игровой объект с автоматическим размером спрайта по объекту
        /// </summary>
        /// <param name="pos">Начальная позиция объекта</param>
        /// <param name="dir">Скорость и направление движения объекта</param>
        /// <param name="size">Размер спрайта объекта</param>
        /// <param name="image">Размер картинки</param>
        public Bullet(Point pos, Point dir, Size size, Image image) : base(pos, dir, size, image) { }

        /// <summary>
        /// Отрисовать объект на экране
        /// </summary>
        public override void Draw()
        {
            if (_image == null)
            {
                Game.Buffer.Graphics.DrawLine(Pens.White, _pos.X, _pos.Y, _size.Width, _size.Height);
            }
            else
            {
                Game.Buffer.Graphics.DrawImage(_image, _pos.X, _pos.Y);
            }
        }

        /// <summary>
        /// Обновить отображение объекта
        /// </summary>
        public override void Update()
        {
            _pos.X = _pos.X + _dir.X;
            if (_pos.X>Program.SCREEN_WIDTH) deleteObjectEvent?.Invoke(this,new GameEventArgs());
        }

        /// <summary>
        /// Метод предназначен для возвращения в начальную позицию
        /// </summary>
        public override void InitStartPosition()
        {
            _pos = new Point(-20, Program.Randomize.Next(20, Program.SCREEN_HEIGHT - 20));
        }
    }
}
