﻿using System;
using System.Drawing;

namespace Asteroids_gbu
{
    abstract class BaseObject : ICollision
    {
        protected Point _pos;
        protected Point _dir;
        protected Size _size;
        protected Image _image;

        /// <summary>
        /// Событие вызывается при удалении объекта
        /// </summary>
        public EventHandler<GameEventArgs> deleteObjectEvent;

        public Point Pos { get => _pos; protected set => _pos = value; }

        //TODO: Удалить после отключения механики логгирования
        public delegate void Logging(LogMessageTypes type, BaseObject data);
        public event Logging AddToLog;
        private static Logger logger = new Logger();
        //\\

        /// <summary>
        /// Создать игровой объект
        /// </summary>
        /// <param name="dir">Скорость и направление движения объекта</param>
        /// <param name="size">Размер игрового объекта</param>
        /// <param name="image">Картинка игрового объекта</param>
        public BaseObject(Point dir, Size size, Image image) : this(new Point(), dir, size, image) { }
        /// <summary>
        /// Создать игровой объект
        /// </summary>
        /// <param name="pos">Начальная позиция объекта</param>
        /// <param name="dir">Скорость и направление движения объекта</param>
        /// <param name="size">Размер игрового объекта</param>
        public BaseObject(Point pos, Point dir, Size size) : this(pos, dir, size, null) { }

        /// <summary>
        /// Создать игровой объект с автоматическим размером объекта по спрайту
        /// </summary>
        /// <param name="pos">Начальная позиция объекта</param>
        /// <param name="dir">Скорость и направление движения объекта</param>
        /// <param name="image">Размер картинки</param>
        public BaseObject(Point pos, Point dir, Image image) : this(pos, dir, image.Size, image) { }

        /// <summary>
        /// Создать игровой объект с автоматическим размером спрайта по объекту
        /// </summary>
        /// <param name="pos">Начальная позиция объекта</param>
        /// <param name="dir">Скорость и направление движения объекта</param>
        /// <param name="size">Размер спрайта объекта</param>
        /// <param name="image">Размер картинки</param>
        public BaseObject(Point pos, Point dir, Size size, Image image)
        {
            _pos = pos;
            _dir = dir;
            _size = size;
            _image = image;

            if (_pos.X < -100
                || _pos.X > Program.SCREEN_WIDTH + 100
                || _pos.Y < -100
                || _pos.Y > Program.SCREEN_HEIGHT + 100)
            {
                //   throw new GameObjectException("Недопустимая позиция объекта!");
            }
            if (_size.Height <= 0 || _size.Width <= 0)
            {
                //   throw new GameObjectException("Недопустимый размер объекта!");
            }

            if (image != null && image.Size != size)
            {
                //размер картинки подгоняе
                _image = (Image)new Bitmap(image, size);
            }

            //TODO: участок для лога, в дальшнейшем убрать
            this.AddToLog += logger.LogToConsole;
            AddToLog(LogMessageTypes.Create, this);
            //\\
        }

        /// <summary>
        /// Прямоугольник для расчета столкновений
        /// </summary>
        public Rectangle Rect => new Rectangle(_pos, _size);

        /// <summary>
        /// Прямоугольник для расчета столкновений
        /// </summary>
        /// <param name="obj">Объект, реализующий ICollision для сравнения</param>
        public bool Collision(ICollision obj)
        {
            //TODO: участок для лога, в дальнейшем убрать
            bool result = this.Rect.IntersectsWith(obj.Rect);
            if (result) AddToLog(LogMessageTypes.Collision, this);
            //\\
            return result;
        }

        /// <summary>
        /// Отрисовать объект на экране
        /// </summary>
        public abstract void Draw();

        /// <summary>
        /// Обновить отображение объекта
        /// </summary>
        public abstract void Update();

        /// <summary>
        /// Метод предназначен для возвращения в начальную позицию
        /// </summary>
        public abstract void InitStartPosition();
    }
}
