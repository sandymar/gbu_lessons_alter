﻿using System;
using System.IO;

namespace Asteroids_gbu
{

    class Logger
    {
        
        public Logger()
        {

        }

        public void LogToConsole(LogMessageTypes type, BaseObject data)
        {
            string forShow = $"{type}: {data.ToString()}, pos = {data.Pos}";
            Console.WriteLine(forShow);
            using (StreamWriter sw = new StreamWriter("log.txt",true))
            {
                sw.WriteLine(forShow);
            }
        }

    }

    enum LogMessageTypes
    {
        Create, Collision, Delete
    }
}
