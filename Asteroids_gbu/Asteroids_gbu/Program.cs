﻿using System;
using System.Windows.Forms;

namespace Asteroids_gbu
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        /// 
        private static SplashScreen splashScreen;
        private static Game gameScreen;
        public static MainForm mainForm;
        public static readonly int SCREEN_WIDTH = Int32.Parse(Properties.Resources.WidthScreen);
        public static readonly int SCREEN_HEIGHT = Int32.Parse(Properties.Resources.HeightScreen);
        public static readonly int UPDATE_INTERVAL = Int32.Parse(Properties.Resources.TimerTick);
        public static readonly Random Randomize = new Random();

        [STAThread]
        static void Main()
        {
            mainForm = new MainForm(OnClick);
            splashScreen = new SplashScreen(mainForm);
            Application.Run(mainForm);
        }

        /// <summary>
        /// Обрабатывает событие нажатия по кнопкам интерфейса
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void OnClick(object sender, EventArgs e)
        {
            string bntName = ((Control)sender).Name;
            switch (bntName)
            {
                case "btnNewGame":
                    splashScreen.Stop();
                    mainForm.showMainInterface(false);
                    mainForm.showGameGUI(true);
                    gameScreen = new Game(mainForm);
                    break;
                case "btnRecords":
                    MessageBox.Show("таблица рекордов тут");
                    break;
                case "btnExit":
                    Application.Exit();
                    break;
                default:
                    break;
            }
        }
    }
}

