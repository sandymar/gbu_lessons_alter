﻿using System.Drawing;

namespace Asteroids_gbu
{
    class Ship : BaseObject
    {
        private int _energy = 100;
        public int Energy { get => _energy; set => _energy = value; }

        /// <summary>
        /// Создать звездолет по умолчанию
        /// </summary>
        public Ship() : base(new Point(), new Point(), new Size(80, 45), Properties.Resources.imgSpaceShip)
        {
            InitStartPosition();
        }

        /// <summary>
        /// Отрисовать объект на экране
        /// </summary>Ы
        public override void Draw()
        {
            Game.Buffer.Graphics.DrawImage(_image, _pos.X, _pos.Y);
        }

        /// <summary>
        /// Обновить отображение объекта
        /// </summary>
        public override void Update(){}
        
        /// <summary>
        /// Метод предназначен для возвращения в начальную позицию
        /// </summary>
        public override void InitStartPosition()
        {
            _dir = new Point(0, 20);
            _pos = new Point(10,Program.SCREEN_HEIGHT / 2);
        }

        /// <summary>
        /// Уменьшение энергии звездолета
        /// </summary>
        /// <param name="n">Количество энергии для уменьшения</param>
        public void EnergyLow(int n)
        {
            _energy -= n;
        }
        
        /// <summary>
        /// Увеличение энергии звездолета
        /// </summary>
        /// <param name="n">Количество энергии для увеличения</param>
        public void EnergyHigh(int n)
        {
            _energy += n;
        }

        /// <summary>
        /// Сместить звездолет вверх
        /// </summary>
        public void Up()
        {
            _pos.Y = _pos.Y - _dir.Y < 0 ? 0 : _pos.Y - _dir.Y;
        }

        /// <summary>
        /// Сместить звездолет вниз
        /// </summary>
        public void Down()
        {
            _pos.Y = _pos.Y + _dir.Y >= Program.SCREEN_HEIGHT-_size.Height ? 
                                        Program.SCREEN_HEIGHT - _size.Height : _pos.Y + _dir.Y;
        }

        /// <summary>
        /// Вызывается при "гибели звездолета" (!!! пока не используется)
        /// </summary>
        public void Die()
        {
        }
    }
}
