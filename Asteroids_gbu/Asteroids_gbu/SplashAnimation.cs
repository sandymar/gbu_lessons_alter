﻿using System.Drawing;

namespace Asteroids_gbu
{
    class SplashAnimation : BaseObject
    {
        /// <summary>
        /// Создать игровой объект
        /// </summary>
        /// <param name="dir">Скорость и направление движения объекта</param>
        /// <param name="size">Размер игрового объекта</param>
        /// <param name="image">Картинка игрового объекта</param>
        public SplashAnimation(Point dir, Size size, Image image) : base(dir, size, image) { InitStartPosition(); }
        /// <summary>
        /// Создать игровой объект
        /// </summary>
        /// <param name="pos">Начальная позиция объекта</param>
        /// <param name="dir">Скорость и направление движения объекта</param>
        /// <param name="size">Размер игрового объекта</param>
        public SplashAnimation(Point pos, Point dir, Size size) : base(pos, dir, size){}
        /// <summary>
        /// Создать игровой объект с автоматическим размером объекта по спрайту
        /// </summary>
        /// <param name="pos">Начальная позиция объекта</param>
        /// <param name="dir">Скорость и направление движения объекта</param>
        /// <param name="image">Размер картинки</param>
        public SplashAnimation(Point pos, Point dir, Image image) : base(pos, dir, image){}
        /// <summary>
        /// Создать игровой объект с автоматическим размером спрайта по объекту
        /// </summary>
        /// <param name="pos">Начальная позиция объекта</param>
        /// <param name="dir">Скорость и направление движения объекта</param>
        /// <param name="size">Размер спрайта объекта</param>
        /// <param name="image">Размер картинки</param>
        public SplashAnimation(Point pos, Point dir, Size size, Image image) : base(pos, dir, size, image){}

        /// <summary>
        /// Отрисовать объект на экране
        /// </summary>
        public override void Draw()
        {
            //отрисовка объекта на указанной позиции
            if (_image == null)
            {
                SplashScreen.Buffer.Graphics.DrawEllipse(Pens.White, _pos.X, _pos.Y, _size.Width, _size.Height);
            }
            else
            {
                if (Program.Randomize.Next(1, 10) == 1) SplashScreen.Buffer.Graphics.DrawEllipse(Pens.Black, _pos.X, _pos.Y, _size.Width, _size.Height);
                else SplashScreen.Buffer.Graphics.DrawImage(_image, _pos.X, _pos.Y);
            }
        }
        /// <summary>
        /// Обновить отображение объекта
        /// </summary>
        public override void Update()
        {
            if (Program.Randomize.Next(1, 10) == 1) _pos = new Point(Program.Randomize.Next(10, Program.SCREEN_WIDTH), Program.Randomize.Next(10, Program.SCREEN_HEIGHT));
        }

        /// <summary>
        /// Метод предназначен для возвращения в начальную позицию
        /// </summary>
        public override void InitStartPosition() {}

    }
}