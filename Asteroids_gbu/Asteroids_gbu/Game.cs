﻿using System;
using System.Windows.Forms;
using System.Drawing;
using System.Collections.Generic;

namespace Asteroids_gbu
{
    class Game : GameWindows
    {
        private int _scores;
        private Ship _ship;
        private List<BaseObject> _objsForClear = new List<BaseObject>();
        private int _asteroidsCount;

        public delegate void UpdateGameStatus(string type, int data);
        public event UpdateGameStatus UpdateStatus;

        /// <summary>
        /// Конструктор игрового окна
        /// </summary>
        /// <param name="form">Форма, куда будет выводиться игровой экран</param>
        public Game(MainForm form) : base(form)
        {
            background = Properties.Resources.imgBackgroung;
            form.KeyDown += FormKeyDown;
            UpdateStatus += form.UpdateGameStatus;
            _asteroidsCount = 2;

            UpdateStatus?.Invoke("Life", _ship.Energy);
            UpdateStatus?.Invoke("Scores", 0);

        }

        /// <summary>
        /// Вызывается при первоначальной загрузке экрана
        /// </summary>
        public override void Load()
        {
            for (int i = 0; i < 50; i++) { _objs.Add(new Star()); }
            _objs.Add(new MedicalKit()); 
            UpdateAsteroids();
            _ship = new Ship();
            _objs.Add(_ship);

        }

        /// <summary>
        /// Заполняет коллекцию объектов для отрисовки астероидами
        /// </summary>
        /// <param name="incCount">Количество, на которое необходимо увеличить количество астероидов. По умолчанию 0</param>
        private void UpdateAsteroids(int incCount = 0)
        {
            _asteroidsCount += incCount;
            for (int i = 0; i < _asteroidsCount; i++)
            {
                Asteroid aster = new Asteroid();
                aster.deleteObjectEvent += deleteObject;
                _objs.Add(aster);
            }
        }


        /// <summary>
        /// ОБновляет позиции всех игровых объектов из коллекции _objs
        /// </summary>
        protected override void Update()
        {
            base.Update();
            //очистка от объектов за границами экрана
            _objsForClear.ForEach(e => _objs.Remove(e));
            _objsForClear.Clear();
            //проверка коллизий
            List<BaseObject> bulletList = _objs.FindAll(e => e is Bullet);
            List<BaseObject> asteroidsList = _objs.FindAll(e => e is Asteroid);
            if (asteroidsList.Count == 0) UpdateAsteroids(1);

            foreach (Asteroid valAsteroid in asteroidsList)
            {
                foreach (Bullet valBullet in bulletList)
                {
                    if (valAsteroid.Collision(valBullet))
                    {
                        valAsteroid.Fired(1);
                        ++_scores;
                        _objsForClear.Add(valBullet);
                    }
                }
                if (valAsteroid.Collision(_ship))
                {
                    valAsteroid.Fired(4);
                    _ship.EnergyLow(10);
                }
            }

            _objs.ForEach(e =>
            {
                if (e is MedicalKit && e.Collision(_ship))
                {
                    e.InitStartPosition();
                    _ship.EnergyHigh(10);
                }
            });

            UpdateStatus?.Invoke("Scores", _scores);
            UpdateStatus?.Invoke("Life", _ship.Energy);
        }

        /// <summary>
        /// Вызывается при нажатии по кнопке на форме
        /// </summary>
        /// <param name="sender">Объект-отправитель</param>
        /// <param name="e">Дополнительные данные</param>
        private void FormKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.ControlKey)
            {
                Bullet bullet = new Bullet(new Point(50, _ship.Pos.Y),
                                           new Point(15, 0),
                                           new Size(10, 10),
                                           Properties.Resources.imgBullet);
                bullet.deleteObjectEvent = deleteObject;
                _objs.Add(bullet);
                //проигрываем звук выстрела
                new System.Media.SoundPlayer(Properties.Resources.fire).Play();
            }

            if (e.KeyCode == Keys.Up) _ship.Up();
            if (e.KeyCode == Keys.Down) _ship.Down();
        }

        /// <summary>
        /// Вызывается по подписке необходимости удалить объект
        /// </summary>
        /// <param name="baseObject">Объект-отправитель</param>
        /// <param name="e">Дополнительные данные</param>
        private void deleteObject(object baseObject, GameEventArgs e)
        {
            _objsForClear.Add((BaseObject)baseObject);
            if (baseObject is Asteroid) new System.Media.SoundPlayer(Properties.Resources.bang).Play();
        }
    }
}
