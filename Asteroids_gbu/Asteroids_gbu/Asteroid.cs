﻿using System;
using System.Drawing;

namespace Asteroids_gbu
{
    class Asteroid : BaseObject
    {
        public int Power { get; set; } = 3;

        /// <summary>
        /// Создать астероид по умолчанию
        /// </summary>
        public Asteroid():base(new Point(),new Point(), new Size(80, 80), Properties.Resources.imgAsteroid)
        {
            InitStartPosition();
        }
        /// <summary>
        /// Создать игровой объект
        /// </summary>
        /// <param name="dir">Скорость и направление движения объекта</param>
        /// <param name="size">Размер игрового объекта</param>
        /// <param name="image">Картинка игрового объекта</param>
        public Asteroid(Point dir, Size size, Image image) : base(dir, size, image) { InitStartPosition(); }
        /// <summary>
        /// Создать игровой объект
        /// </summary>
        /// <param name="pos">Начальная позиция объекта</param>
        /// <param name="dir">Скорость и направление движения объекта</param>
        /// <param name="size">Размер игрового объекта</param>
        public Asteroid(Point pos, Point dir, Size size) : base(pos, dir, size){}
        /// <summary>
        /// Создать игровой объект с автоматическим размером объекта по спрайту
        /// </summary>
        /// <param name="pos">Начальная позиция объекта</param>
        /// <param name="dir">Скорость и направление движения объекта</param>
        /// <param name="image">Размер картинки</param>
        public Asteroid(Point pos, Point dir, Image image) : base(pos, dir, image){}
        /// <summary>
        /// Создать игровой объект с автоматическим размером спрайта по объекту
        /// </summary>
        /// <param name="pos">Начальная позиция объекта</param>
        /// <param name="dir">Скорость и направление движения объекта</param>
        /// <param name="size">Размер спрайта объекта</param>
        /// <param name="image">Размер картинки</param>
        public Asteroid(Point pos, Point dir, Size size, Image image) : base(pos, dir, size, image){}

        /// <summary>
        /// Отрисовать объект на экране
        /// </summary>
        public override void Draw()
        {
            if (_image == null)
            {
                Game.Buffer.Graphics.DrawEllipse(Pens.White, _pos.X, _pos.Y, _size.Width, _size.Height);
            }
            else
            {
                Game.Buffer.Graphics.DrawImage(_image,_pos.X, _pos.Y,_image.Size.Width/4*Power, _image.Size.Height / 3 * Power);
            }
        }

        /// <summary>
        /// Обновить отображение объекта
        /// </summary>
        public override void Update()
        {
            //проверка позиции с поправкой на размер объекта
            int rightPos = _pos.X + _size.Width;
            int bottomPos = _pos.Y + _size.Height;

            //дополнительно корректируем позции по x и y для исклчюение "заползания за экран"
            if (_pos.X+ _size.Width<= 0)
            {
                InitStartPosition();
            }
            else if (_pos.Y <= 0)
            {
                _dir.Y = -_dir.Y;
                _pos.Y = 0;
            }
            else if (bottomPos >= Program.SCREEN_HEIGHT)
            {
                _dir.Y = -_dir.Y;
                _pos.Y = Program.SCREEN_HEIGHT - _size.Height;
            }

            _pos.X = _pos.X + _dir.X;
            _pos.Y = _pos.Y + _dir.Y;

            _image.RotateFlip(RotateFlipType.Rotate90FlipNone);
        }


        /// <summary>
        /// Метод предназначен для возвращения в начальную позицию
        /// </summary>
        public override void InitStartPosition()
        {
            Power = 3;
            _dir = new Point(Program.Randomize.Next(-8, -2), Program.Randomize.Next(-10, 10));
            _pos = new Point(Program.SCREEN_WIDTH+_size.Width, Program.Randomize.Next(40, Program.SCREEN_HEIGHT));
        }

        /// <summary>
        /// Метод для учета попадания по астероиду
        /// </summary>
        /// <param name="n">Сила попадания</param>
        public void Fired(int n)
        {
            Power-=n;
            if (Power <= 0) { deleteObjectEvent?.Invoke(this, new GameEventArgs()); };
        }
    }
}