﻿using System.Drawing;

namespace Asteroids_gbu
{
    class MedicalKit : BaseObject
    {
        /// <summary>
        /// Создать аптечку по умолчанию
        /// </summary>
        public MedicalKit() : base(new Point(), new Point(), new Size(45, 30), Properties.Resources.imgMedicalKit)
        {
            InitStartPosition();
        }

        /// <summary>
        /// Создать игровой объект
        /// </summary>
        /// <param name="dir">Скорость и направление движения объекта</param>
        /// <param name="size">Размер игрового объекта</param>
        /// <param name="image">Картинка игрового объекта</param>
        public MedicalKit(Point dir, Size size, Image image) : base(dir, size, image){}

        /// <summary>
        /// Создать игровой объект
        /// </summary>
        /// <param name="pos">Начальная позиция объекта</param>
        /// <param name="dir">Скорость и направление движения объекта</param>
        /// <param name="size">Размер игрового объекта</param>
        public MedicalKit(Point pos, Point dir, Size size) : base(pos, dir, size){}

        /// <summary>
        /// Создать игровой объект с автоматическим размером объекта по спрайту
        /// </summary>
        /// <param name="pos">Начальная позиция объекта</param>
        /// <param name="dir">Скорость и направление движения объекта</param>
        /// <param name="image">Размер картинки</param>
        public MedicalKit(Point pos, Point dir, Image image) : base(pos, dir, image){}

        /// <summary>
        /// Создать игровой объект с автоматическим размером спрайта по объекту
        /// </summary>
        /// <param name="pos">Начальная позиция объекта</param>
        /// <param name="dir">Скорость и направление движения объекта</param>
        /// <param name="size">Размер спрайта объекта</param>
        /// <param name="image">Размер картинки</param>
        public MedicalKit(Point pos, Point dir, Size size, Image image) : base(pos, dir, size, image){}

        /// <summary>
        /// Отрисовать объект на экране
        /// </summary>
        public override void Draw()
        {
            if (_image == null)
            {
                Game.Buffer.Graphics.DrawLine(Pens.White, _pos.X, _pos.Y, _pos.X + _size.Width, _pos.Y + _size.Height);
                Game.Buffer.Graphics.DrawLine(Pens.White, _pos.X + _size.Width, _pos.Y, _pos.X, _pos.Y + _size.Height);
            }
            else
            {
                Game.Buffer.Graphics.DrawImage(_image, _pos);
            }
        }

        /// <summary>
        /// Обновить отображение объекта
        /// </summary>
        public override void Update()
        {
            _pos.X = _pos.X + _dir.X;
            if (_pos.X + _size.Width < 0) InitStartPosition();
        }

        /// <summary>
        /// Метод предназначен для возвращения в начальную позицию
        /// </summary>
        public override void InitStartPosition()
        {
            _dir = new Point(Program.Randomize.Next(-10, -2), 0);
            _pos = new Point(Program.SCREEN_WIDTH, Program.Randomize.Next(10, Program.SCREEN_HEIGHT-_size.Height));
        }
    }
}
