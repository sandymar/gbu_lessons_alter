﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Asteroids_gbu
{
    abstract class GameWindows
    {
        public static int Width { get; private set; }
        public static int Height { get; private set; }
        public static BufferedGraphics Buffer;
        protected static BufferedGraphicsContext _context;
        protected static Timer timer;

        protected List<BaseObject> _objs;
        protected Image background { get; set; } = default(Image);

        /// <summary>
        /// Конструктор игрового окна
        /// </summary>
        /// <param name="form">Форма, куда будет выводиться игровой экран</param>
        public GameWindows(Form form)=>init(form);

        /// <summary>
        /// Вызывается при первоначальной загрузке экрана
        /// </summary>
        public abstract void Load();

        /// <summary>
        /// Инициализация формы
        /// </summary>
        /// <param name="form">Форма, куда будет выводиться игровой экран</param>
        public void init (Form form)
        {
            Graphics g;
            _context = BufferedGraphicsManager.Current;
            g = form.CreateGraphics();
            Width = form.ClientSize.Width;
            Height = form.ClientSize.Height;

            if (Width > 1000) { throw new ArgumentOutOfRangeException(); }
            else if (Height > 1000) { throw new ArgumentOutOfRangeException(); }
            else if (Width < 0 || Height < 0) { throw new ArgumentOutOfRangeException(); };

            Buffer = _context.Allocate(g, new Rectangle(0, 0, Width, Height));
            _objs = new List<BaseObject>();

            Load();

            timer = new Timer();
            timer.Interval = Program.UPDATE_INTERVAL;
            timer.Tick += onTimerTick;
            timer.Start();
        }

        /// <summary>
        /// Отрисовывает все игровые объекты из коллекции _objs
        /// </summary>
        private void Draw()
        {
            Buffer.Graphics.Clear(Color.Black);

            if (background!=null) Buffer.Graphics.DrawImage(Properties.Resources.imgBackgroung, new Rectangle(0, 0, Width, Height));

            foreach (BaseObject obj in _objs)
            {
                obj.Draw();
            }
            Buffer.Render();
        }

        /// <summary>
        /// ОБновляет позиции всех игровых объектов из коллекции _objs
        /// </summary>
        protected virtual void Update()
        {
            foreach (BaseObject obj in _objs)
            {
                obj.Update();
            }
        }


        /// <summary>
        /// Вызывается для обновления экрана
        /// </summary>
        private void onTimerTick(object sender, EventArgs e)
        {
            Draw();
            Update();
        }

        /// <summary>
        /// Прекратить использование игрового экрана и очистить ресурсы
        /// </summary>
        public void Stop()
        {
            timer.Stop();
            _objs = null;
            _context = null;
            Buffer = null;
        }

    }
}