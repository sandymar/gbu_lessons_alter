﻿using System.Drawing;
using System.Windows.Forms;

namespace Asteroids_gbu
{
    class SplashScreen:GameWindows
    {

        /// <summary>
        /// Конструктор игрового окна
        /// </summary>
        /// <param name="form">Форма, куда будет выводиться игровой экран</param>
        public SplashScreen(Form form) : base(form){}

        /// <summary>
        /// Вызывается при первоначальной загрузке экрана
        /// </summary>
        public override void Load()
        {
            for (int i = 0; i<60; i++)
            {
                Point pos = new Point(Program.Randomize.Next(10, Program.SCREEN_WIDTH), Program.Randomize.Next(10, Program.SCREEN_HEIGHT));
                Point dir = new Point(0, 0);
                SplashAnimation splashAnimation = new SplashAnimation(pos, dir, new Size(20, 20), Properties.Resources.imgStar);
                _objs.Add(splashAnimation);
            }
        }
    }
}
