﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Asteroids_gbu
{
    class MainForm :Form
    {
        private Button btnNewGame, btnRecords, btnExit, btnBack;
        private Label lbTitle, lbAuthor, lbScores;
        private ProgressBar pbLifeBar;

        ///<summary>
        ///Класс основной игровой формы
        ///</summary>
        public MainForm(EventHandler onClickListener) : base() {
            initForm(onClickListener);
        }

        ///<summary>
        ///Инициализирует и показывает основную игровую форму
        ///</summary>
        private void initForm(EventHandler onClickListener)
        {
            this.Text = "Asteroids game";
            //Именно ClientSize, т.к. size - размер вместе с заголовками
            //Отключаем изменение размеров пользователем и центрируем окно
            this.ClientSize = new Size(Program.SCREEN_WIDTH, Program.SCREEN_HEIGHT);
            this.FormBorderStyle = FormBorderStyle.FixedDialog;
            this.MinimizeBox = false;
            this.MaximizeBox = false;
            this.StartPosition = FormStartPosition.CenterScreen;
            this.KeyPreview = true;

            //готовим управляющие элементы меню            
            btnNewGame = CreateNewButton("Новая игра",0,0,"btnNewGame");
            btnRecords = CreateNewButton("Рекорды", 50, 0, "btnRecords");
            btnExit    = CreateNewButton("Выход", 100, 0,"btnExit");

            btnBack    = CreateNewButton("Назад",0,0, "btnToMainMenu");
            btnBack.Size = new Size(80, 20);
            btnBack.Left = Program.SCREEN_WIDTH - btnBack.Size.Width;
            btnBack.Top = Program.SCREEN_HEIGHT - btnBack.Size.Height;

            btnNewGame.Click += onClickListener;
            btnRecords.Click += onClickListener;
            btnExit.Click    += onClickListener;

            lbTitle = new Label();
            lbTitle.Text = "Астероиды";
            lbTitle.Font = new Font(lbTitle.Font.FontFamily, 36, FontStyle.Bold);
            lbTitle.TextAlign = ContentAlignment.MiddleCenter;
            lbTitle.ForeColor = Color.Coral;
            lbTitle.BackColor = Color.Black;
            lbTitle.Size = new Size(300, 40);
            lbTitle.Left = Program.SCREEN_WIDTH / 2 - (lbTitle.Size.Width / 2);
            lbTitle.Top  = Program.SCREEN_HEIGHT / 2 - (lbTitle.Size.Height*3);

            lbAuthor = new Label();
            lbAuthor.Text = "Автор: SandyMar";
            lbAuthor.Font = new Font(lbTitle.Font.FontFamily, 12);
            lbAuthor.TextAlign = ContentAlignment.MiddleCenter;
            lbAuthor.ForeColor = Color.Coral;
            lbAuthor.BackColor = Color.Black;
            lbAuthor.Size = new Size(300, 40);
            lbAuthor.Left = Program.SCREEN_WIDTH / 2 - (lbAuthor.Size.Width / 2);
            lbAuthor.Top  = Program.SCREEN_HEIGHT - 40;

            //нужна кнопка возврата в меню, отображение очков и лайфбар
            lbScores = new Label();
            lbScores.Name = "lbScores";
            lbScores.Text = "weqweqweqwe";
            lbScores.Font = new Font(lbTitle.Font.FontFamily, 18);
            lbScores.TextAlign = ContentAlignment.MiddleCenter;
            lbScores.ForeColor = Color.Coral;
            lbScores.BackColor = Color.Black;
            lbScores.Size = new Size(300, 40);
            lbScores.Left = Program.SCREEN_WIDTH / 2 - (lbAuthor.Size.Width / 2);
            lbScores.Top = 0;

            pbLifeBar = new ProgressBar();
            pbLifeBar.Name = "pbLifeBar";
            pbLifeBar.Maximum = 100;
            pbLifeBar.Value = 30;
            pbLifeBar.Size = new Size(123,20);
            pbLifeBar.Step = 10;
            pbLifeBar.BackColor = Color.Black;
            pbLifeBar.ForeColor = Color.Coral;
            pbLifeBar.Left = Program.SCREEN_WIDTH/2-pbLifeBar.Size.Width/2;
            pbLifeBar.Top  = Program.SCREEN_HEIGHT - pbLifeBar.Size.Height;

            this.Controls.Add(btnNewGame);
            this.Controls.Add(btnRecords);
            this.Controls.Add(btnExit);
            this.Controls.Add(lbTitle);
            this.Controls.Add(lbAuthor);
            this.Controls.Add(lbScores);
            this.Controls.Add(pbLifeBar);

            this.BackColor = Color.Black;
            this.ShowIcon = false;
            this.Show();

            showMainInterface(true);
            showGameGUI(false);
        }

        /// <summary>
        /// Возвращает новую кнопку интерфейса
        /// </summary>
        /// <param name="text">Текст кнопки</param>
        /// <param name="centerMarignY">Отступ от центра по Y</param>
        /// <param name="centerMarignX">Отступ от центра по X</param>
        /// <returns></returns>
        private Button CreateNewButton(string text, int centerMarignY, int centerMarignX, String Name)
        {
            Button btnNew = new Button();
            btnNew.Size = new Size(150, 40);
            btnNew.Left = Program.SCREEN_WIDTH / 2 - (btnNew.Size.Width / 2) + centerMarignX;
            btnNew.Top = Program.SCREEN_HEIGHT / 2 - (btnNew.Size.Height / 2) + centerMarignY;
            btnNew.Text = text;
            btnNew.FlatStyle = FlatStyle.Flat;
            btnNew.BackColor = Color.LightBlue;
            btnNew.Name = Name;
            return btnNew;
        }

        /// <summary>
        /// Отобразить/скрыть интерфейс приложения
        /// </summary>
        /// <param name="state">Признак видимости интерфейса</param>
        public void showMainInterface(bool state)
        {
            btnExit.Enabled = state;
            btnExit.Visible = state;

            btnNewGame.Enabled = state;
            btnNewGame.Visible = state;

            btnRecords.Enabled = state;
            btnRecords.Visible = state;

            lbAuthor.Enabled = state;
            lbAuthor.Visible = state;

            lbTitle.Enabled = state;
            lbTitle.Visible = state;
        }

        /// <summary>
        /// Отобразить элементы игрового экрана
        /// </summary>
        /// <param name="state">Признак видимости и доступности</param>
        public void showGameGUI(bool state)
        {
            lbScores.Enabled = state;
            lbScores.Visible = state;

            pbLifeBar.Enabled = state;
            pbLifeBar.Visible = state;
        }

        /// <summary>
        /// Обновить данные элементов интерфейса. Вызывается по подписке
        /// </summary>
        /// <param name="type">Название обновляемого поля</param>
        /// <param name="data">Новое значение поля</param>
        public void UpdateGameStatus(string type, int data)
        {
            data = data < 0 ? 0 : data;
            if (type == "Scores") lbScores.Text = $"Набрано очков: {data}";
            if (type == "Life") pbLifeBar.Value = pbLifeBar.Maximum < data? pbLifeBar.Maximum: data;
        }
    }
}